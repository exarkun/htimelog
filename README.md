Building
========

```sh
nix build --impure .#desktop # build the desktop frontend
nix build --impure .#android # build the android app
```

Development / Testing
=====================

```sh
# Get a development shell
nix develop --impure

# Run the web frontend on http://localhost:8181/ w/ live reload
ob run

# Run the test suite
cabal test frontend --test-show-details=direct --test-option=--format=progress
```
