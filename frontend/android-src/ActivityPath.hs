module ActivityPath where

import qualified Android.HaskellActivity as Android

getFilesDir :: IO (Maybe FilePath)
getFilesDir = Android.getHaskellActivity >>= Android.getFilesDir
