module ActivityPath where

import System.Environment.XDG.BaseDir (getUserDataDir)

getFilesDir :: IO (Maybe FilePath)
getFilesDir =
    Just <$> getUserDataDir "htimelog"
