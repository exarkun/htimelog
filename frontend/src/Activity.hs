{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}

module Activity
  ( Activity(..)
  , getCurrentActivities
  , getCurrentActivitiesFromPath
  , activityEndedUTC
  , computeDuration
  , addActivity
  , persist
  , activityP
  , formatZonedTime
  , iso8601P
  , activityAge
  ) where

import System.Directory
  ( createDirectoryIfMissing
  )
import System.FilePath
  ( takeDirectory
  )

import GHC.IO.Exception
  ( IOException(IOError, ioe_type)
  , IOErrorType(NoSuchThing)
  )

import Control.Exception
  ( catch
  , throwIO
  )

import Data.List.NonEmpty
  ( NonEmpty((:|))
  )

import Data.Void
  ( Void
  )
import Data.Time.Format
  ( formatTime
  )
import Text.Megaparsec
  ( Parsec
  , ParseErrorBundle
  , ErrorItem(Tokens)
  , parse
  , many
  , satisfy
  , failure
  , takeP
  , option
  )

import Text.Megaparsec.Char
  ( eol
  , string
  )

import qualified Data.Text as T
import qualified Data.Text.IO as T
import Data.Time
  ( UTCTime(..)
  , LocalTime(..)
  , NominalDiffTime
  , diffUTCTime
  )
import Data.Time.LocalTime
  ( ZonedTime(..)
  , zonedTimeToUTC
  )
import Data.Time.Format
  ( parseTimeM
  , defaultTimeLocale
  )

import System.IO
  ( withFile
  , IOMode(AppendMode)
  )

type Parser = Parsec Void T.Text
type ParseError = ParseErrorBundle T.Text Void

data Activity = Activity
  { activityEnded :: ZonedTime
  , activityDescription :: T.Text
  } deriving (Show)

instance Eq Activity where
  left == right = activityKey left == activityKey right

instance Ord Activity where
  compare left right = compare (activityKey left) (activityKey right)

activityKey :: Activity -> (UTCTime, T.Text)
activityKey act = (zonedTimeToUTC . activityEnded $ act, activityDescription act)


getCurrentActivitiesFromPath :: FilePath -> IO (Either ParseError [Activity])
getCurrentActivitiesFromPath path =
  getCurrentActivities path (T.pack <$> readFile path)

getCurrentActivities :: FilePath -> IO T.Text -> IO (Either ParseError [Activity])
getCurrentActivities path contents =
  (parse activitiesP path <$> contents) `catch` \(e@IOError { ioe_type }) -> do
  case ioe_type of
    NoSuchThing -> return $ Right []
    _           -> throwIO e

activitiesP :: Parser [Activity]
activitiesP = do
  -- ignore a BOM
  _ <- option "" (string "\65279")
  many $ activityP <* eol

iso8601P :: Parser ZonedTime
iso8601P = do
  ts <- T.unpack <$> takeP (Just "timestamp") 19
  tz <- T.unpack <$> takeP (Just "timezone") 7
  case ZonedTime <$> parseTimeM False defaultTimeLocale "%Y-%m-%d %H:%M:%S" ts <*> parseTimeM False defaultTimeLocale "%z: " tz of
    Nothing -> failure (Just (Tokens (head ts :| tail ts <> tz))) mempty
    Just t -> return t

activityP :: Parser Activity
activityP = do
  activityEnded <- iso8601P
  activityDescription <- T.pack <$> (many $ satisfy ('\n' /=))
  return Activity {..}

addActivity :: FilePath -> Activity -> IO ()
addActivity path activity =
  addIt `catch` \(e :: IOException) -> do
    putStrLn ("addActivity '" ++ path ++ "' failed: " ++ show e)
    throwIO e
  where
    addIt = do
      createDirectoryIfMissing True (takeDirectory path)
      (withFile path AppendMode $ \h -> T.hPutStr h (persist activity))

persist :: Activity -> T.Text
persist Activity { activityEnded, activityDescription } =
  persisted
  where
    persisted = T.concat pieces
    pieces = [formatZonedTime activityEnded, activityDescription, "\n"]


formatZonedTime :: ZonedTime -> T.Text
formatZonedTime = T.pack . formatTime defaultTimeLocale "%Y-%m-%d %H:%M:%S%z: "

-- | Compute a DiffTime denoting the duration of an activity, given that
-- activity and the one before it.
--
-- The first activity of a day has a duration of zero.  All other activities
-- have a duration equal to the difference between their end time and the
-- previous activity's end time.
computeDuration :: Activity -> Activity -> NominalDiffTime
computeDuration prev cur
  | sameDay   = if sameDayDuration < 0 then error details else sameDayDuration
  | otherwise = 0
  where
    sameDay = zonedDay prevEnded == zonedDay curEnded
    zonedDay = localDay . zonedTimeToLocalTime
    prevEnded = activityEnded prev
    curEnded = activityEnded cur

    sameDayDuration = diffUTCTime (activityEndedUTC cur) (activityEndedUTC prev)
    details = show prev <> " -> " <> show cur <> ": " <> show sameDayDuration

activityEndedUTC :: Activity -> UTCTime
activityEndedUTC = zonedTimeToUTC . activityEnded

-- | Calculate the difference between the given time and the time when the
-- given activity ended.
activityAge :: ZonedTime -> Activity -> NominalDiffTime
activityAge whence Activity { activityEnded } =
    diffUTCTime (zonedTimeToUTC whence) (zonedTimeToUTC activityEnded)
