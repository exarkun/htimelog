{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NamedFieldPuns #-}

module Report where

import Data.Tuple.Extra
  ( uncurry3
  )

import Data.Maybe
  ( fromMaybe
  )

import qualified Data.Text as T
import qualified Data.Map.Strict as M

import Activity
  ( Activity(..)
  , activityEndedUTC
  , computeDuration
  , getCurrentActivitiesFromPath
  )

import Data.Either
  ( fromRight
  )

import Data.Time
  ( UTCTime(..)
  , ZonedTime(..)
  , LocalTime(..)
  , TimeOfDay(..)
  , TimeZone
  , NominalDiffTime
  , utcToLocalTime
  , toGregorian
  , fromGregorian
  , zonedTimeToUTC
  , getZonedTime
  , utcToZonedTime
  , addUTCTime
  , utc
  , localTimeToUTC
  )

type Year = Integer
type MonthOfYear = Int
type DayOfMonth = Int

otherProject :: T.Text
otherProject = "Other"

slackingProject :: T.Text
slackingProject = "Slacking"

defaultProjects :: [T.Text]
defaultProjects =
  [ "farm"
  , "aspiration"
  , "lae - Product Development - PrivateStorage"
  , "lae - DRL B.A.S.E. Project"
  , "lae - Product Development - Winden/magic-wormhole"
  , "lae - MoonMath Manual"
  ]

projectTotals :: [T.Text] -> [Activity] -> M.Map T.Text NominalDiffTime
projectTotals _projects     [] = mempty
projectTotals  projects (a:as) =
  projectTotals' $ zip durations (a:as)
  where
    durations = 0:zipWith computeDuration (a:as) as

    projectTotals' :: [(NominalDiffTime, Activity)] -> M.Map T.Text NominalDiffTime
    projectTotals' =
      foldr (\(duration, Activity { activityDescription }) accum ->
                let
                  proj = findProject projects activityDescription
                in
                  M.alter (Just . (duration +) . fromMaybe 0) proj accum
            ) mempty

findProject :: [T.Text] -> T.Text -> T.Text
findProject     [] description = if "**" `T.isSuffixOf` description then slackingProject else otherProject
findProject (p:ps) description =
  if T.isPrefixOf p description
  then p
  else findProject ps description

-- | A predicate for activities that ended betwen the first and second times.
activityBetween :: ZonedTime -> ZonedTime -> Activity -> Bool
activityBetween begin end Activity { activityEnded } =
  z >= (zonedTimeToUTC begin) && z < (zonedTimeToUTC end)
  where
    z = zonedTimeToUTC activityEnded

-- | A predicate like activityBetween but with boundaries given in UTC.
activityBetweenUTC :: UTCTime -> UTCTime -> Activity -> Bool
activityBetweenUTC begin end a@Activity { activityEnded = ZonedTime { zonedTimeZone } } =
    activityBetween (localize begin) (localize end) a
    where
      localize = utcToZonedTime zonedTimeZone

-- | A predicate for activities that ended in the same month as the given
-- time.
activityInMonth :: ZonedTime -> Activity -> Bool
activityInMonth when Activity { activityEnded } =
  -- XXX What if they're in different timezones?
  key when == key activityEnded
  where
    key zonedTime =
      let (year, month, _day) = toGregorian . localDay . zonedTimeToLocalTime $ zonedTime
      in (year, month)

-- | Did the activity end during the calendar month before the calendar month
-- of the given time?
activityInPreviousMonth :: ZonedTime -> Activity -> Bool
activityInPreviousMonth = activityInMonth . lastMonthZoned

-- | Construct a Day one month earlier than the given Day.
lastMonth :: LocalTime -> LocalTime
lastMonth LocalTime { localDay, localTimeOfDay } =
  LocalTime (uncurry3 fromGregorian . lastMonth' . toGregorian $ localDay) localTimeOfDay
  where
    lastMonth' :: (Year, MonthOfYear, DayOfMonth) -> (Year, MonthOfYear, DayOfMonth)
    lastMonth' (year, 1, day) = (year - 1, 12, day)
    lastMonth' (year, month, day) = (year, month - 1, day)


-- | Return the first moment of the calendar month containing the given time.
startOfMonthZoned :: ZonedTime -> ZonedTime
startOfMonthZoned zt =
    zt { zonedTimeToLocalTime = start }
    where
      start = LocalTime { localDay = startDay, localTimeOfDay = TimeOfDay 0 0 0 }

      startDay = fromGregorian year month 1
      (year, month, _) = toGregorian . localDay . zonedTimeToLocalTime $ zt

-- | A version of lastMonth that works with ZonedTime
lastMonthZoned :: ZonedTime -> ZonedTime
lastMonthZoned z@ZonedTime { zonedTimeToLocalTime } =
  z { zonedTimeToLocalTime = lastMonth zonedTimeToLocalTime }

lastMonthActivities :: FilePath -> IO [Activity]
lastMonthActivities p =
  filter . activityInPreviousMonth <$> getZonedTime <*> (fromRight [] <$> getCurrentActivitiesFromPath p)

-- | Did the activity end within the week ending at the given time?
activityInLastWeek :: ZonedTime -> Activity -> Bool
activityInLastWeek now activity =
  addUTCTime (-oneWeek) (zonedTimeToUTC now) < (activityEndedUTC activity)

oneWeek :: NominalDiffTime
oneWeek = 60 * 60 * 24 * 7

data Report =
    Report
    { reportTimeZone :: TimeZone
    , reportStartTime :: LocalTime
    , reportEndTime :: LocalTime
    , reportWorkTime :: NominalDiffTime
    , reportSlackTime :: NominalDiffTime
    , reportProjectTimes :: M.Map T.Text NominalDiffTime
    } deriving (Show, Ord, Eq)

report :: (NominalDiffTime -> NominalDiffTime -> M.Map T.Text NominalDiffTime -> Report) -> [Activity] -> Report
report makeReport activities =
  makeReport (workTime totals) (slackTime totals) totals
  where
    totals = projectTotals defaultProjects activities

    workTime :: M.Map T.Text NominalDiffTime -> NominalDiffTime
    workTime = M.foldrWithKey (\k v accum -> if k == otherProject || k == slackingProject then accum else v + accum) 0

    slackTime :: M.Map T.Text NominalDiffTime -> NominalDiffTime
    slackTime = fromMaybe 0 . M.lookup slackingProject

reportForInterval :: LocalTime -> LocalTime -> TimeZone -> [Activity] -> Report
reportForInterval startTime endTime timeZone activities =
    report (Report timeZone startTime endTime) (filter (activityBetween (ZonedTime startTime timeZone) (ZonedTime endTime timeZone)) activities)

-- | Get the report for the calendar month prior to the one in which the given
-- time falls.
lastMonthReport :: ZonedTime -> [Activity] -> Report
lastMonthReport now activities =
    reportForInterval (zonedTimeToLocalTime start) (zonedTimeToLocalTime end) (zonedTimeZone now) activities
    where
      end = startOfMonthZoned $ now
      start = lastMonthZoned end

-- | Get the report for the seven days ending in the given time.
lastWeekReport :: ZonedTime -> [Activity] -> Report
lastWeekReport now activities =
    reportForInterval start end (zonedTimeZone now) activities
    where
      end = zonedTimeToLocalTime now
      start = addLocalTime (-oneWeek) end

-- | addLocalTime d t = t + d
addLocalTime :: NominalDiffTime -> LocalTime -> LocalTime
addLocalTime x = utcToLocalTime utc . addUTCTime x . localTimeToUTC utc

reportForInterval' :: NominalDiffTime -> ZonedTime -> [Activity] -> Report
reportForInterval' interval intervalEnd activities =
    reportForInterval start end zone activities
    where
      end = zonedTimeToLocalTime intervalEnd
      start = addLocalTime (-interval) end
      zone = zonedTimeZone intervalEnd
