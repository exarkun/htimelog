{-# LANGUAGE CPP #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NamedFieldPuns #-}

module Frontend where

import Data.Maybe
  ( fromMaybe
  )

import Control.Applicative
    ( liftA2
    )
import Control.Monad
  ( void
  )

import Text.Read (readEither)
import qualified Data.Map as M
import Data.Either
  ( fromRight
  )

import Data.Time.LocalTime
    ( LocalTime(..)
    , getCurrentTimeZone
    , utcToZonedTime
    , utc
    )
import Data.Time
  ( UTCTime(..)
  , NominalDiffTime
  , Day(ModifiedJulianDay)
  , ZonedTime(..)
  , TimeOfDay(..)
  , Day(..)
  )
import qualified Data.Time
  ( getCurrentTime
  )

import qualified Data.Time.Format
import Control.Exception
  ( IOException
  , Exception
  , catch
  , try
  , throwIO
  )
import Control.Monad.IO.Class
  ( MonadIO
  , liftIO
  )
import Control.Monad.Fix (MonadFix)
import qualified Data.Text as T
import System.IO.Unsafe (unsafePerformIO)

import Obelisk.Frontend
import Obelisk.Route
import Obelisk.Generated.Static

import Reflex.Dom
  ( Performable
  , TriggerEvent
  , DomBuilder
  , inputElement
  , dropdown
  , _dropdown_value
  , MonadHold(holdDyn),
    PerformEvent(performEvent),
    Reflex(current, Event, Dynamic),
    EventTag(KeypressTag),
    InputElement(_inputElement_value),
    EventName(Keyup, Click, Keypress),
    PostBuild(getPostBuild),
    (&),
    (.~),
    accumDyn,
      ffilter,
      ffor,
      filterLeft,
      filterRight,
      leftmost,
      tag,
      traceEvent,
      traceEventWith,
      zipDynWith,
      attachPromptlyDynWith,
      tickLossy,
      inputElementConfig_setValue,
      (=:),
      blank,
      dyn,
      dynText,
      dyn_,
      el,
      el',
      elAttr,
      elClass,
      elDynAttr',
      elDynClass,
      elDynClass',
      text,
      def,
      TickInfo(TickInfo, _tickInfo_lastUTC),
      HasDomEvent(..) )

import Common.Route

import Report
    ( Report(..)
    , lastWeekReport
    , lastMonthReport
    , findProject
    , defaultProjects
    , reportForInterval'
    )

import Activity
  ( Activity(..)
  , getCurrentActivitiesFromPath
  , addActivity
  , computeDuration
  , activityAge
  )

import ActivityPath (getFilesDir)

type FiniteActivity = (NominalDiffTime, Activity)

data Modifier = Ctrl | Alt | Shift deriving (Show, Ord, Eq)

getDataPath :: String -> IO (Maybe FilePath)
getDataPath basename = ((<> "/" <> basename) <$>) <$> getFilesDir

-- This runs in a monad that can be run on the client or the server.
-- To run code in a pure client or pure server context, use one of the
-- `prerender` functions.
frontend :: Frontend (R FrontendRoute)
frontend = Frontend
  { _frontend_head = do
      el "title" $ text "Time Tracking"
      elAttr "link" ("href" =: $(static "main.css") <> "type" =: "text/css" <> "rel" =: "stylesheet") blank

  , _frontend_body = do
      clockDyn <- makeClockDyn
      rootElement clockDyn
  }

-- | An event that occurs once, close to the time the app starts, with the
-- time at that moment.
appStartTimeEvent :: (PostBuild t m, PerformEvent t m, MonadIO (Performable m)) => m (Event t ZonedTime)
appStartTimeEvent = do
  postBuild <- getPostBuild
  performEvent $ ffor postBuild $ \_ -> liftIO $
                    utcToZonedTime <$> Data.Time.LocalTime.getCurrentTimeZone <*> Data.Time.getCurrentTime

makeClockDyn :: (PostBuild t m, PerformEvent t m, TriggerEvent t m, MonadIO (Performable m), MonadFix m, MonadHold t m) => m (Dynamic t ZonedTime)
makeClockDyn = do
  let
      -- Before the page build event and before the first tick fires, we don't
      -- really know what time it is.  That should be a very short window so
      -- maybe this fine...  However, we still need a value (or we need to
      -- expose the entire rest of the UI to a `Maybe ZonedTime` ...).  Since
      -- it is a short window, just make up a time.  It will make the reports
      -- wrong, it will make the current activity duration wrong, it will make
      -- anything that depends on the current time wrong.  But ... it will
      -- also almost immediately be corrected.  I'd like to remove this wart
      -- but I don't know how.
      tz = Data.Time.LocalTime.utc
      epoch = UTCTime (ModifiedJulianDay 1) 1
      zonedEpoch = utcToZonedTime tz epoch
  tickEvent <- tickLossy 60 epoch
  zonedEvent <- performEvent $ ffor tickEvent (liftIO . tickInfoToZonedTime)
  appStartTime <- appStartTimeEvent
  holdDyn zonedEpoch (leftmost [ appStartTime, zonedEvent ])


tickInfoToZonedTime :: TickInfo -> IO ZonedTime
tickInfoToZonedTime TickInfo { _tickInfo_lastUTC } = do
  tz <- Data.Time.LocalTime.getCurrentTimeZone
  return $ utcToZonedTime tz _tickInfo_lastUTC

data HTimelogError = DataPathUnavailable deriving (Eq, Ord, Show)

instance Exception HTimelogError

rootElement :: (MonadFix m, MonadHold t m, PostBuild t m, DomBuilder t m, PerformEvent t m, MonadIO (Performable m)) => Dynamic t ZonedTime -> m ()
rootElement clockDyn =
  let
    activityPath :: IO FilePath
    activityPath =
        getDataPath "htimelog.activities" >>= maybe (throwIO DataPathUnavailable) pure

    -- XXX Move IO to the edges, propagate this information in an Event or a Dynamic
    activityPath' = unsafePerformIO activityPath

    visibleIf :: Reflex t => (a -> Bool) -> Dynamic t a -> Dynamic t T.Text
    visibleIf cond = ((\x -> if cond x then "content-visible" else "content-hidden") <$>)
  in
    do
      rec
        let
          getCurrentActivities = getCurrentActivitiesFromPath activityPath' `catch` \(ex :: IOException) -> do
            n <- Data.Time.getCurrentTime
            tz <- getCurrentTimeZone
            return $ Right [Activity (utcToZonedTime tz n) (T.pack $ "Error loading activities: " ++ show ex)]

          -- Accumulate some attributes to apply to the root element.
          --
          -- Giving it a tabindex allows it to receive focus at all which is
          -- necessary for our + / - font change handling.
          rootAttrsDyn = (("class" =: "root") <>). (("tabindex" =: "1") <>) . ("style" =:) <$> fontStyleDyn

          -- CSS for styling (sizing, specifically) fonts
          fontStyleDyn = T.pack . (++ "px") . ("font-size: " ++) . show <$> fontSizeDyn

          -- The current font size is the starting font size plus the number of
          -- Ctrl-+s minus the number of Ctrl--s.
          fontSizeDyn = (10 +) <$> ((-) <$> fontSizeIncreaseDyn <*> fontSizeDecreaseDyn)

        fontSizeIncreaseDyn <- countDyn (traceEvent "+" $ keyPressEvent [Ctrl] '+' rootElement)
        fontSizeDecreaseDyn <- countDyn (traceEvent "-" $ keyPressEvent [Ctrl] '-' rootElement)

        -- Put all of the interesting pieces into an overall root element that
        -- we can attach handlers for otherwise unhandled events to.
        (rootElement, ()) <- elDynAttr' "div" rootAttrsDyn $ do
          cp <- controlPanel

          el "hr" blank

          (_, activitiesDyn :: Dynamic t [Activity]) <- elDynClass' "div" (visibleIf (Activities ==) cp) $
                                                             activities clockDyn getCurrentActivities activityPath'

          elDynClass "div" (visibleIf (Reporting ==) cp) $ do
            repo <- dynamicReport clockDyn activitiesDyn
            dyn_ $ repo
            el "hr" blank
            dyn_ $ renderReport <$> (lastWeekReport <$> clockDyn <*> activitiesDyn)
            el "hr" blank
            dyn_ $ renderReport <$> (lastMonthReport <$> clockDyn <*> activitiesDyn)

          return ()

      return ()

-- | Render an activity report for a user-input-controlled interval.  This
-- renders both the user input widgets and the report into the output.
dynamicReport ::
    ( DomBuilder t m
    , PostBuild t m
    , MonadHold t m
    , MonadFix m
    ) => Dynamic t ZonedTime
    -> Dynamic t [Activity]
    -> m (Dynamic t (m ()))
dynamicReport clockDyn activitiesDyn  = do
  reportingPeriodIntervalDyn <- el "div" nominalDiffTimeInput
  reportingPeriodEndDayDyn <- el "div" calendarDayInput

  let reportingPeriodEndLocalTime = LocalTime <$> reportingPeriodEndDayDyn <*> pure (TimeOfDay 0 0 0)
      reportingPeriodEndDyn = ZonedTime <$> reportingPeriodEndLocalTime <*> (zonedTimeZone <$> clockDyn)

  pure $ renderReport <$> (reportForInterval' <$> reportingPeriodIntervalDyn <*> reportingPeriodEndDyn <*> activitiesDyn)

data IntervalUnit = Day | Week | Month | Year deriving (Eq, Ord, Show)

-- | Return the number of seconds spanned by the given unit.
secondsForUnit :: IntervalUnit -> NominalDiffTime
secondsForUnit Day = 60 * 60 * 24
secondsForUnit Week = 60 * 60 * 24 * 7
secondsForUnit Month = 60 * 60 * 24 * 7 * 30
secondsForUnit Year = 60 * 60 * 24 * 7 * 365

-- | Render a user input element which supplies a time interval value.  The
-- Dynamic updates with every single parseable input of the element.
nominalDiffTimeInput :: (DomBuilder t m, PostBuild t m, MonadHold t m, MonadFix m) => m (Dynamic t NominalDiffTime)
nominalDiffTimeInput = el "span" $ do
  numericEl <- inputElement def
  text "Report Period"

  unitEl <- dropdown Day (pure $ M.fromList [(Day, "days"), (Week, "weeks"), (Month, "months"), (Year, "years")]) def

  let numInputDyn = _inputElement_value numericEl
      numOutputDyn = fromIntegral . intFromText <$> numInputDyn
      unitOutputDyn = secondsForUnit <$> _dropdown_value unitEl

  pure $ zipDynWith (*) unitOutputDyn numOutputDyn

  where
    intFromText :: T.Text -> Int
    intFromText inp =
        case readEither . T.unpack $ inp of
          Left _err -> 0
          Right x -> x

-- | Render a user input element which supplies a number of days.  The Dynamic
-- updates with every single parseable input of the element.
calendarDayInput :: DomBuilder t m => m (Dynamic t Day)
calendarDayInput = el "span" $ do
  inpEl <- inputElement def
  text "Report Period End (Julian day #)"
  let inputDyn = _inputElement_value inpEl
      outputDyn = daysFromText <$> inputDyn
  pure outputDyn
  where
    daysFromText :: T.Text -> Day
    daysFromText inp = ModifiedJulianDay days
        where
          days = case readEither . T.unpack $ inp of
                Left _err -> 0
                Right x -> x


-- | Keep track of the number of times an event has fired in a Dynamic.
countDyn :: (Reflex t, MonadHold t m, MonadFix m) => Event t a -> m (Dynamic t Int)
countDyn = accumDyn (\a _b -> a + 1) 0

keyPressEvent ::
  ( Reflex t
  , Enum a
  , Enum (DomEventType target 'KeypressTag)
  , HasDomEvent t target 'KeypressTag
  ) => p -> a -> target -> Event t ()
keyPressEvent _modifiers key =
  -- XXX implement modifiers
  fmap (const ()) . ffilter ((fromEnum key ==) . fromEnum) . domEvent Keypress

data Content = Activities | Reporting deriving (Show, Ord, Eq)

-- | Create a set of inputs that control what content is visible.
controlPanel :: (DomBuilder t m, MonadHold t m) => m (Dynamic t Content)
controlPanel = do
  elClass "div" "control-panel-buttons" $ do
      (activity, _) <- el' "button" $ text "Activity"
      (reports, _) <- el' "button" $ text "Reports"

      holdDyn Activities $ leftmost
          [ Activities <$ domEvent Click activity
          , Reporting <$ domEvent Click reports
          ]

-- | Display an input for entering a new activity and recently entered
-- activities.
activities :: (DomBuilder t m, PostBuild t m, MonadFix m, PerformEvent t m, MonadIO (Performable m), MonadHold t m) => Dynamic t ZonedTime -> IO (Either e [Activity]) -> FilePath -> m (Dynamic t [Activity])
activities clockDyn getCurrentActivities activityPath = do
  persistedActivitiesDyn <- activityInput clockDyn getCurrentActivities (addActivity activityPath)
  activityOutput clockDyn persistedActivitiesDyn
  pure persistedActivitiesDyn

-- | Display recent activities.
--
-- Full detail is displayed for activities within the past 24 hours.  For
-- activities older than that but newer than 7 days, all activities are shown
-- but activities with the same description are merged.  This is repeated
-- again for activities between 7 days and 1 month old and then for each month
-- past that.
--
-- XXX Under construuuuction
activityOutput
    :: (Monad m, Reflex t, DomBuilder t m, PostBuild t m)
    => Dynamic t ZonedTime
    -> Dynamic t [Activity]
    -> m ()
activityOutput clockDyn persistedActivitiesDyn =
    -- Put the list of divs, each containing a description of an activity,
    -- into the page.
    void $ el "div" $ dyn activityDivDyn
    where
      -- Activities divided into 24 hours, 7 days, 1 month, N months
      dividedActivities = zipDynWith divideActivities clockDyn activitiesNewestFirst

      -- Activities with durations in newest to oldest order.
      activitiesNewestFirst = reverse . allDurations <$> persistedActivitiesDyn

      divideActivities :: ZonedTime ->
                          [FiniteActivity] ->
                         ( [FiniteActivity]
                         , [FiniteActivity]
                         , [FiniteActivity]
                         , [[FiniteActivity]]
                         )
      divideActivities whence allActivities =
          (oneDayActivities, oneWeekActivities, oneMonthActivities, nMonthActivities)
          where
            (oneDayActivities, olderActivities) = span (youngerThan oneDay whence . snd) allActivities
            (oneWeekActivities, olderActivities') = span (youngerThan oneWeek whence . snd) olderActivities
            (oneMonthActivities, olderActivities'') = span (youngerThan oneMonth whence . snd) olderActivities'
            nMonthActivities = [olderActivities'']



      youngerThan :: NominalDiffTime -> ZonedTime -> Activity -> Bool
      youngerThan age whence which = age > activityAge whence which

      oneDay :: NominalDiffTime
      oneDay = 60 * 60 * 24

      oneWeek :: NominalDiffTime
      oneWeek = oneDay * 7

      oneMonth :: NominalDiffTime
      oneMonth = oneDay * 30

      -- `Dynamic t [a]` of divs of rendered activities.
      activityDivDyn = mapM (uncurry formatActivity) <$> activitiesNewestFirst

-- | Accept input describing new activities.
activityInput
    :: (DomBuilder t m, MonadFix m, PostBuild t m, PerformEvent t m, MonadIO (Performable m), MonadHold t m)
    => Dynamic t ZonedTime
    -> IO (Either e [Activity])
    -> (Activity -> IO ())
    -- A Dynamic of persisted activities.
    -> m (Dynamic t [Activity])
activityInput clockDyn getCurrentActivities addActivity' = do
  rec
    -- Put the current activity timer and a description text input into the
    -- page.  The text input will be reset (to empty) whenever the new
    -- activity is saved.  This leaves the field populated if there is an
    -- error during save so the content isn't lost.  The activity timer counts
    -- up from the end time of the most recent saved activity.

    (_, activityDescInput) <- el' "div" $ do
      elClass "span" "current-duration" $ dynText currentDurationTextDyn

      elClass "span" "current-description" $ inputElement $ def
          & inputElementConfig_setValue .~ ("" <$ savedActivityEvent)
    let
      -- `Dynamic t T.Text` holding the value of the text input.
      activityDescValue = _inputElement_value activityDescInput

      -- `Behavior t T.text` holding the current value of the text input.
      activityDescBehavior = current activityDescValue

       -- `Event t (DomEventType target eventName)` that fires whenever
       -- there's a "keyup" event on the text input.
      keyupEvent = domEvent Keyup activityDescInput

       -- `Event t (DomEventType target eventName)` that fires whenever the
       -- keyup event is for enter (13).
      enterEvent = ffilter (\keycode -> keycode == 13) keyupEvent

      -- `Event t T.Text` that fires with the text in the text input whenever
      -- enter is released.
      activityDescEvent' = tag activityDescBehavior enterEvent

      -- `Event t T.Text` that fires with the text in the text input whenever
      -- enter is released and the input is not empty.
      activityDescEvent = ffilter ((> 0) . T.length) activityDescEvent'

      -- `Event t Activity` that fires whenever a non-empty activity description
      -- is input.  The Activity includes the time when the event fires.
      activityEvent = attachPromptlyDynWith Activity clockDyn activityDescEvent

    -- An event which occurs after the widget is built.
    postBuildEvent <- getPostBuild

    -- An event which occurs whenever we should reload and re-render persisted
    -- activities.  This will be either whenever the widget is built or we
    -- save a new activity.  We could extend this list with an event that
    -- occurs whenever the file is externally modified, too (detected by, eg,
    -- inotify or simple mtime polling).
    let reloadActivitiesEvent = leftmost [postBuildEvent, savedActivityEvent]

    -- An event which occurs with a [Activity] value giving all persisted
    -- activities.  It fires whenever the reload event says it should.
    --
    -- XXX `fromRight []` means we throw parse errors in the trash...
    persistedActivitiesEvent <- performEvent $ ffor reloadActivitiesEvent $ \_ ->
      liftIO $ fromRight [] <$> getCurrentActivities

    -- `Dynamic t [Activity]` giving the most recent persisted activities list
    -- ([] before it is known).
    persistedActivitiesDyn <- holdDyn [] persistedActivitiesEvent

    -- Compute the duration of the current activity, updated once a minute.
    latestActivityDyn <- holdDyn Nothing (lastMaybe <$> persistedActivitiesEvent)
    let
      currentDurationTextDyn = fromMaybe "" . (formatDuration <$>) <$> zipDynWith (liftA2 currentActivityDuration) latestActivityDyn (Just <$> clockDyn)

    -- `Event t (Either IOException ())` that tries to write a new activity
    -- whenever one is created.
    savingActivityEvent <-
      traceEventWith (("saving: " ++) . show) <$>
      (performEvent $
       ffor activityEvent $
       liftIO . (try :: IO a -> IO (Either IOException a)) . addActivity')

    let
      -- `Event t ()` that occurs whenever a new activity was successfully
      -- recorded.
      savedActivityEvent = traceEventWith (("saved: " ++) . show ) (filterRight savingActivityEvent)

      -- `Event t IOException` that occurs whenever an attempt to record an
      -- activity fails.
      savingActivityFailed = traceEventWith (("failed: " ++) . show) (filterLeft savingActivityEvent)

    let
    -- `Dynamic t T.Text` of the last error that happened while trying to save
    -- activities.
    errorDyn <- holdDyn "" ((\(e :: IOException) -> T.pack $ show e) <$> savingActivityFailed)

    -- `Dynamic t T.Text` of a visible or hidden class.  A failed save
    -- switches to visible.  A successful save switches to invisible.
    errorClassDyn <- holdDyn "error-hidden" (leftmost ["error-hidden" <$ savedActivityEvent, "" <$ savingActivityFailed])

    void $ el "div" $ do
      -- Put a div where we can drop error information into the page.
      void $ elDynClass "div" errorClassDyn $ do
        el "span" $ text "Error saving activities: "
        -- XXX If you accidentally give `el "span"` an `m (Dynamic ...)` it type
        -- checks but misbehaves at runtime. :(
        el "span" $ dynText errorDyn

  return persistedActivitiesDyn

modalErrorReport :: Monad m => s -> m ()
modalErrorReport _msg = return ()

formatActivity :: DomBuilder t m => NominalDiffTime -> Activity -> m ()
formatActivity duration Activity { activityEnded = when, activityDescription = what } =
    el "div" $ do
      elClass "span" "activity-end" $ text (formatTime when)
      elClass "span" "activity-duration" $ text (formatDuration duration)
      elClass "div" "activity-description" $
          let
            (proj, desc) = splitActivityProject defaultProjects what
          in
            text $ T.concat ["(", proj, ") ", desc]

splitActivityProject :: [T.Text] -> T.Text -> (T.Text, T.Text)
splitActivityProject projects fullDescription =
  (project', description')
  where
    project = findProject projects fullDescription
    (projectCandidate, description) = T.splitAt (T.length project) fullDescription
    (project', description') =
      if projectCandidate == project
      then
        -- Found it
        (projectCandidate, description)
      else
        -- Found some default, preserve all the info
        (project, fullDescription)

-- | Annotate each activity with its duration - the time from the previous
-- activity's end to its own end, accounting for a "first activity of the day"
-- which is calculated differently to account for the fact that the activity
-- before it was probably "sleeping" and should be disregarded.
allDurations :: [Activity] -> [(NominalDiffTime, Activity)]
allDurations activities' = zipWith (\prev cur -> (computeDuration prev cur, cur)) activities' (tail activities')

lastMaybe :: [a] -> Maybe a
lastMaybe [] = Nothing
lastMaybe xs = Just . last $ xs

formatTime :: ZonedTime -> T.Text
formatTime = T.pack . Data.Time.Format.formatTime Data.Time.Format.defaultTimeLocale "%Y-%m-%d %H:%M"

formatDuration :: NominalDiffTime -> T.Text
formatDuration duration =
  T.concat (T.pack <$> [show h, "h ", show m, "m"])
  where
    minutes = floor duration `div` 60 :: Int
    m = minutes `mod` 60
    h = minutes `div` 60

currentActivityDuration :: Activity -> ZonedTime -> NominalDiffTime
currentActivityDuration latestActivity currentTime =
  computeDuration latestActivity (Activity currentTime "current activity")

-- | Render a summary of time spent on an activity.
formatProjectTime :: DomBuilder t m => (T.Text, NominalDiffTime) -> m ()
formatProjectTime (projectName, projectTotal) = do
  el "div" $ do
    elClass "span" "project-total" $ text $ formatDuration projectTotal
    text " "
    elClass "span" "project-name" $ text projectName

reportLabel :: Report -> T.Text
reportLabel Report { reportTimeZone, reportStartTime, reportEndTime } =
    formatTime (ZonedTime reportStartTime reportTimeZone)
                   <> " - "
                   <> formatTime (ZonedTime reportEndTime reportTimeZone)

renderReport :: DomBuilder t m => Report -> m ()
renderReport report = do
  el "div" $ do
    el "div" $ text $ reportLabel report
    el "div" $ text $ ("Worked: " <>) . formatDuration . reportWorkTime $ report
    el "div" $ text $ ("Slacked: " <>) . formatDuration . reportSlackTime $ report
    el "span" $ mapM_ formatProjectTime . M.toList . reportProjectTimes $ report
