{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import           Hedgehog.Main
  ( defaultMain
  )
import qualified Data.Text as T

import Text.Megaparsec
  ( parse
  )

import Data.List
  ( sort
  )

import Data.Time
  ( UTCTime(..)
  , LocalTime(..)
  , TimeZone(..)
  , ZonedTime(..)
  , NominalDiffTime
  , DiffTime
  , TimeOfDay(..)
  , secondsToDiffTime
  , addUTCTime
  , diffUTCTime
  , utcToZonedTime
  , zonedTimeToUTC
  )

import Data.Time.Calendar
  ( fromGregorian
  , toGregorian
  )

import Activity
  ( Activity(..)
  , getCurrentActivities
  , persist
  , activityP
  , computeDuration
  , formatZonedTime
  , iso8601P
  )

import Frontend
  ( currentActivityDuration
  , allDurations
  )

import Report
  ( projectTotals
  , lastMonthZoned
  , activityInMonth
  , activityInPreviousMonth
  )

import Control.Monad.IO.Class
  ( liftIO
  )

import Reflex

genUTCTime :: MonadGen m => m UTCTime
genUTCTime = do
    y <- toInteger <$> Gen.int (Range.constant 2000 2019)
    m <- Gen.int (Range.constant 1 12)
    d <- Gen.int (Range.constant 1 28)
    let day = fromGregorian y m d
    secs <- toInteger <$> Gen.int (Range.constant 0 86401)
    let diff = secondsToDiffTime secs
    pure $ UTCTime day diff

genTimeZone :: MonadGen m => m TimeZone
genTimeZone = do
  offset <- Gen.int (Range.constant (-12 * 60 + 1) (12 * 60))
  TimeZone offset <$> Gen.bool <*> pure (show offset)

genZonedTime :: MonadGen m => m ZonedTime
genZonedTime = do
  utc <- genUTCTime
  tz <- genTimeZone
  pure $ utcToZonedTime tz utc

genZonedTimeInMonth :: MonadGen m => ZonedTime -> m ZonedTime
genZonedTimeInMonth someTime = do
  -- Build some arbitrary time on some arbitrary day in some unspecified
  -- month.  This will replace the day and time in some arbitrary ZonedTime to
  -- produce a new ZonedTime that is frequently not equal to the original but
  -- is in the same month.
  someDay <- Gen.int (Range.linear 1 28)
  someHour <- Gen.int (Range.linear 0 23)
  someMinute <- Gen.int (Range.linear 0 59)
  -- Let's disregard leap seconds for now...
  someSec <- Gen.int (Range.linear 0 59)

  let
    (year, month, day) = toGregorian . localDay . zonedTimeToLocalTime $ someTime

  pure $ ZonedTime
      (LocalTime
        (fromGregorian year month someDay)
        (TimeOfDay someHour someMinute $ fromIntegral someSec))
      (zonedTimeZone someTime)

genNominalDiffTime :: MonadGen m => m NominalDiffTime
genNominalDiffTime = toEnum <$> Gen.integral (Range.linear 0 1000000000)

genActivity :: MonadGen m => m Activity
genActivity =
  Activity
  <$> genZonedTime
  <*> genDescription
  where
    genDescription =
      Gen.text (Range.linear 0 100) (Gen.filterT (/= '\n') Gen.unicode)

prop_roundtripZonedTime :: Property
prop_roundtripZonedTime = property $ do
  -- ZonedTime has no Eq instance so compare the serialized form.
  zonedTime <- forAll genZonedTime
  let serialized = formatZonedTime zonedTime
  tripping serialized (parse iso8601P "test") (formatZonedTime <$>)

prop_roundtripActivity :: Property
prop_roundtripActivity = property $ do
  activity <- forAll genActivity
  tripping activity persist (parse activityP "test")

prop_getCurrentActivities :: Property
prop_getCurrentActivities =
  property $ do
  original <- forAll $ Gen.list (Range.linear 0 1000) genActivity
  let
    serialized = T.concat . map persist $ original
  annotateShow serialized
  roundtripped <- liftIO $ getCurrentActivities "tests" $ return serialized
  diff (Right original) (==) roundtripped

prop_computeDuration :: Property
prop_computeDuration =
  property $ do
  prevTime <- forAll $ genZonedTime
  expected <- forAll $ genNominalDiffTime

  let
    previousAct = Activity prevTime ""
    currentAct = Activity (addZonedTime expected prevTime) ""
    actual = computeDuration previousAct currentAct

  diff expected (==) actual

prop_currentActivityDuration :: Property
prop_currentActivityDuration =
  property $ do
  someTime <- forAll genZonedTime
  expectedInterval <-forAll $ genNominalDiffTime
  let
    latestActivity = Activity someTime ""
    actual = currentActivityDuration
      latestActivity
      (addZonedTime expectedInterval someTime)

  diff expectedInterval (==) actual

-- | The sum of the totals across all projects must equal the sum of the
-- duration of each activity.
prop_projectTotals :: Property
prop_projectTotals =
  property $ do
  jumbledActivities <- forAll $ Gen.list (Range.linear 2 100) genActivity
  let
    activities = sort jumbledActivities
    firstActivity = head activities
    lastActivity = last activities

  projects <- forAll $ Gen.list (Range.linear 0 5) (Gen.text (Range.linear 0 10) Gen.ascii)

  let
    totals = projectTotals projects activities
    actualSum = foldr (+) 0 totals

    expectedSum =
      foldr (\(duration, _activity) accum -> accum + duration) 0 (allDurations activities)

  annotateShow totals
  diff expectedSum (==) actualSum

-- | lastMonthZoned returns a date that is earlier than the one passed to
-- it.
prop_lastMonthZoned :: Property
prop_lastMonthZoned =
  property $ do
  later <- forAll $ genZonedTime
  let
    earlier = lastMonthZoned later

  diff (zonedTimeToUTC earlier) (<) (zonedTimeToUTC later)

-- | activityInMonth returns True if the month of the UTCTime equals the month
-- of the end time of the Activity.
prop_activityInMonth_True :: Property
prop_activityInMonth_True =
  property $ do
  someActivity <- forAll $ genActivity
  someOtherTime <- forAll $ genZonedTimeInMonth (activityEnded someActivity)
  diff True (==) (activityInMonth someOtherTime someActivity)

-- | activityInMonth returns False if the month of the ZonedTime does not not
-- equal the month of the end time of the activity.
prop_activityInMonth_False :: Property
prop_activityInMonth_False =
  property $ do
  someTime <- forAll $ genZonedTime

  let
    someActivity = Activity someTime ""
    earlierTime = lastMonthZoned someTime

  diff False (==) (activityInMonth earlierTime someActivity)

-- | activityInPreviousMonth returns True if the month of the ZonedTime equals
-- the month of the end time of the activity.
prop_activityInPreviousMonth_True :: Property
prop_activityInPreviousMonth_True =
  property $ do
  someTime <- forAll genZonedTime
  let
    someEarlierTime = lastMonthZoned someTime
    someActivity = Activity someEarlierTime ""

  diff True (==) (activityInPreviousMonth someTime someActivity)

-- | activityInPreviousMonth returns False if the month of the ZonedTime does
-- not equal the month of the end time of the activity.
prop_activityInPreviousMonth_False :: Property
prop_activityInPreviousMonth_False =
  property $ do
  someActivity <- forAll genActivity
  someTime <- forAll $ genZonedTimeInMonth (activityEnded someActivity)
  diff False (==) (activityInPreviousMonth someTime someActivity)

tests :: IO Bool
tests =
  checkParallel $ Group "Activity"
  [ ("prop_roundtripActivity", prop_roundtripActivity)
  , ("prop_getCurrentActivities", prop_getCurrentActivities)
  , ("prop_computeDuration", prop_computeDuration)
  , ("prop_currentActivityDuration", prop_currentActivityDuration)
  , ("prop_projectTotals", prop_projectTotals)
  , ("prop_lastMonthZoned", prop_lastMonthZoned)
  , ("prop_activityInMonth_True", prop_activityInMonth_True)
  , ("prop_activityInMonth_False", prop_activityInMonth_False)
  , ("prop_activityInPreviousMonth_True", prop_activityInPreviousMonth_True)
  , ("prop_activityInPreviousMonth_False", prop_activityInPreviousMonth_False)
  , ("prop_roundtripZonedTime", prop_roundtripZonedTime)
  ]

addZonedTime :: NominalDiffTime -> ZonedTime -> ZonedTime
addZonedTime diff zt =
  utcToZonedTime (zonedTimeZone zt) (addUTCTime diff (zonedTimeToUTC zt))

main = defaultMain [tests]
