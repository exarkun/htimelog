{
  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    flake-utils = {
      url = "github:numtide/flake-utils";
    };
    obelisk-src = {
      url = "github:obsidiansystems/obelisk?rev=41f97410cfa2e22a4ed9e9344abcd58bbe0f3287";
      flake = false;
    };
    reflex-platform-src = {
      url = "github:reflex-frp/reflex-platform?rev=123a6f487ca954fd983f6d4cd6b2a69d4c463d10";
      flake = false;
    };
  };
  outputs = {
    self,
    flake-utils,
    obelisk-src,
      reflex-platform-src,
      ...
  }: let
    systemDependent =
      flake-utils.lib.eachSystem [
        flake-utils.lib.system.x86_64-linux
      ] (system: let
        # Customize the reflex-platform used by obelisk.  Presently there are
        # no customizations except to pin the source revision (instead of
        # relying on obelisk's reflex-platform thunk).
        reflex-platform-func = args: import reflex-platform-src (args // {
        });

        obelisk = import obelisk-src {
          inherit system reflex-platform-func;

          iosSdkVersion = "13.2";

          # You must accept the Android Software Development Kit License
          # Agreement at https://developer.android.com/studio/terms in order
          # to build Android apps.  Uncomment and set this to `true` to
          # indicate your acceptance:
          config.android_sdk.accept_license = true;

          # In order to use Let's Encrypt for HTTPS deployments you must
          # accept their terms of service at
          # https://letsencrypt.org/repository/.  Uncomment and set this to
          # `true` to indicate your acceptance:
          terms.security.acme.acceptTerms = true;
        };
      in rec {

        # Get the whole Obelisk project project.  This is *not* exposed
        # beneath `packages` or another standard flake output field because it
        # isn't structured to fit in any of those places.  It will still be
        # available as a flake output but as its own unknown top-level value.
        # Below we'll pull some values out of it that do fit into the standard
        # flake output fields.
        obelisk_project = obelisk.project ./. ({...}: {
          android = {
            applicationId = "com.jcalderone.htimelog";
            displayName = "HTimelog";

            version = {
              # Must be a monotonically increasing number; defines what it
              # means to "upgrade" the app
              code = "4";

              # The version that is displayed to the end user
              name = "1.0";
            };

            # Could ask for extra permissions at install-time like this.
            #
            # permissions = ''
            # <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
            # <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
            # <uses-permission android:name="android.permission.MANAGE_DOCUMENTS"/>
            # '';

            # Could split the build into one per system instead of bundling
            # everything into a single apk.  It saves a bunch of megabytes but
            # it breaks the deploy script.
            #
            # universalApk = false;

            # Among other things, a release key is required to be allowed to *upgrade*
            # an installed app.
            #
            # XXX This leaks all the secrets into the nix store !!
            # releaseKey = {
            #   storeFile = /home/exarkun/Secrets/Credentials/keytool-keystore;
            #   storePassword = "password";
            #   keyAlias = "magnon";
            #   keyPassword = "password";
            # };
          };

          # Sounds nice in principle but increases build time substantially.
          withHoogle = false;

          # XXX Try to put a nice ghcid into dev shell environment ... but
          # not convinced this actually did anything useful for us.
          shellToolOverrides = ghc: super: {
            # XXX How do we get the right pkgs.haskell.lib here?  `nixpkgs` is
            # probably right since it's the nixpkgs we got from obelisk/reflex
            # ... but it would be nicer to pull things from the `ghc` or
            # `super` parameter.
            ghcid = nixpkgs.haskell.lib.justStaticExecutables super.ghcid;
            inherit
              (ghc)
              haskell-language-server
              fourmolu
              ;

            inherit (obelisk) command;
          };
        });

        # Expose the whole nixpkgs package set that Obelisk / Reflex are
        # using.  Anything that needs to interoperate with pieces built by
        # Obelisk needs to use this package set the pieces (Haskell libraries,
        # native libraries, whatever) have a chance of being compatible.
        nixpkgs = obelisk_project.reflex.nixpkgs;

        packages = {
          # The Android application - this includes the apk.
          android = obelisk_project.android.frontend;

          # A native (desktop) build of some of the frontend - but it seems to
          # be missing pieces so it's not clear how useful this is by itself.
          exe = obelisk_project.exe;

          # Supposedly, an Android emulator configured to run the app.  In
          # practice, it dies before starting the app with some kind of llvm
          # error.
          emulator = nixpkgs.androidenv.emulateApp {
            name = "emulate-HTimelog";
            platformVersion = "24";
            abiVersion = "armeabi-v7a"; # mips, x86, x86_64, armeabi-v7a
            systemImageType = "default";
            app = "${obelisk_project.android.frontend}/android-app-debug.apk";
            package = "HTimelog";
            activity = "MainActivity";
          };

          # A native (desktop) build of the frontend.  This one actually
          # works.
          desktop = obelisk_project.ghc.frontend;
        };

        # Supply a development environment that includes various Haskell tools
        # like ghc, cabal, haskell-language-server, etc.
        devShells.default = obelisk_project.shells.ghc;
      });
  in
    systemDependent;
}
